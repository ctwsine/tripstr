//
//  ProfileViewController.h
//  tripstr
//
//  Created by ctwsine on 3/17/14.
//  Copyright (c) 2014 ctwsine. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UserModel.h"

@interface ProfileViewController : UIViewController

@property (nonatomic,strong) UserModel* author;


@end
